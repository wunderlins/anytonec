# AnytoneC

## Proof of concepts

### Config

 - [QSettings configuration class][1]

### https

 - [Console example][2]
 - windows ssl, get binaries [here][4]. copy the following files to your runtime directory
   - libcrypto-1_1-x64.dll
   - libssl-1_1-x64.dll
   - **CAVE**: might need MS redist with which openssl was built.
	 

### Json

 - [Json][3]


### Map

 - ...

### geo

 - [QGeoCoordinate Class][5]
 - [QGeoPolygon Class][6]



[1]: https://doc.qt.io/qt-5/qsettings.html
[2]: https://stackoverflow.com/questions/25433706/qt-console-app-using-qnetworkaccessmanager
[3]: https://doc.qt.io/qt-5/json.html 
[4]: http://slproweb.com/products/Win32OpenSSL.html
[5]: https://doc.qt.io/qt-5/qgeocoordinate.html#details
[6]: https://doc.qt.io/qt-5/qgeopolygon.html
