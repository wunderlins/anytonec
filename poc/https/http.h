#ifndef HTTP_H
#define HTTP_H

#include <QObject>

#include <QDebug>
#include <QtCore>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>

#include <QStandardPaths>

class http : public QObject
{
Q_OBJECT
    QNetworkAccessManager *manager;
    QFile *file;
    bool success = false;
    bool in_progress = false;
    QNetworkReply* reply;
    QNetworkRequest request;
    QUrl qrl;

private slots:
    void replyFinished(QNetworkReply *);
    void error(QNetworkReply::NetworkError code);
    void uploadProgress(qint64 bytesSent, qint64 bytesTotal);
    void readyRead();
    //void abort();

public:
    explicit http(QObject *parent = nullptr);
    void download(QString url, QFile *file);
};

#endif // HTTP_H
