#include <QCoreApplication>
#include "http.h"

QString createDataPath() {
    QString location = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(location);
    if (!dir.exists()) {
        if (!dir.mkpath(location)) {
            qDebug() << "Error, failed to create data direcotry";
            return nullptr;
        }
    }

    return location;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString url("https://api.brandmeister.network/v1.0/repeater/?action=LIST");

    QString location = createDataPath();
    if (location == nullptr) {
        return 1;
    }

    location.append("/repeater_list.json");
    QFile f(location);

    qDebug() << "Downloading repeater list";
    qDebug() << " - From: " << url;
    qDebug() << " - To:   " << location;

    http handler;
    handler.download(url, &f);

    return a.exec();
}
