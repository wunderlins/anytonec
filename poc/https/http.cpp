#include "http.h"

http::http(QObject *parent) : QObject(parent)
{

}

void http::replyFinished(QNetworkReply *reply)
{
    //file->write(reply->readAll());
    file->close();
    in_progress = false;

    qDebug() << "Downloaded and saved " << file->size() << "bytes.";

    // cleanup unused objects
    reply->deleteLater();
    file->deleteLater();
}

void http::error(QNetworkReply::NetworkError code)
{
    file->close();
    success = false;
    in_progress = false;

    qDebug() << "A network error occoured: " << code << ": " << reply->errorString();
}

void http::uploadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    qDebug() << bytesSent << ", " << bytesTotal;
}

void http::readyRead()
{
    qDebug() << "readyRead()";
    qDebug() << file->write(reply->readAll());
}

void http::download(QString url, QFile *f)
{
  file = f;
  success = true;

  bool open = file->open(QIODevice::WriteOnly);
  if (!open) {
      file->close();
      in_progress = false;
      success = false;
      qDebug() << "Failed to open: "  << file->fileName();
      return;
  }

  qrl.setUrl(url);
  manager = new QNetworkAccessManager(this);
  manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);

  connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
  //connect(request, SIGNAL(abort()), this, SLOT(abort()));

  request = QNetworkRequest(url);
  request.setRawHeader("User-Agent", "AnytoneC 1.0");

  reply = manager->get(request);
  connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
  connect(reply, SIGNAL(uploadProgress(qint64, qint64)),
          this, SLOT(uploadProgress(qint64, qint64)));
  connect(reply, SIGNAL(readyRead()),
          this, SLOT(readyRead()));
}

