#ifndef MAIN_H
#define MAIN_H

#include <memory>
#include <QCoreApplication>
#include <string>
#include <chrono>
#include <ctime>
#include <map>

#include <QGeoCoordinate>
#include <QGeoPolygon>

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

#include <QStandardPaths>
#include <QDebug>
#include <QDir>

/**
 * repeater object
 *
 * {
 *  "repeaterid": "21300022",
 *  "callsign": "",
 *  "hardware": "Windows:BlueDV",
 *  "firmware": "1.0.0.9551-DVMEGA_HR3.26",
 *  "tx": "439.9500",
 *  "rx": "439.9500",
 *  "colorcode": "1",
 *  "status": "4",
 *  "lastKnownMaster": "2141",
 *  "lat": "42.506001",
 *  "lng": "1.518100",
 *  "city": "Somewhere",
 *  "website": "www.pa7lim.nl",
 *  "pep": null,
 *  "gain": null,
 *  "agl": "1",
 *  "priorityDescription": null,
 *  "description": null,
 *  "last_updated": "2019-07-09 20:29:37"
 * },
 */
class Repeater {

public:
    Repeater(int id): repeaterid(id) {}
    int repeaterid; // ": "21300022",
    QString callsign; //": "",
    QString hardware; //": "Windows:BlueDV",
    QString firmware; //: "1.0.0.9551-DVMEGA_HR3.26",
    double tx; // "439.9500
    double rx; // "439.9500
    int colorcode; // "1",
    int status; // "4",
    int lastKnownMaster; // "2141",

    float lat; // "42.506001",
    float lng; // "1.518100",

    QGeoCoordinate loc;

    QString city; // "Somewhere",
    QString website; // "www.pa7lim.nl",
    QString pep; // null,
    QString gain; // null,
    int agl; //"1",
    QString priorityDescription; // null,
    QString description; // null,
    std::tm last_updated; // "2019-07-09 20:29:37"
};

#endif // MAIN_H
