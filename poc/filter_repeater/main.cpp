#include "main.h"
#include "repeaterlistparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // json file is located in the application directory
    QString location = createDataPath();
    if (location == nullptr) {
        return 1;
    }
    location.append("/repeater_list.json");
    QFile f(location);

    // parse the repeater list
    repeaterListParser j;
    RptrList repeater;
    RptrList r = j.loadJson(&f, repeaterListParser::Json, repeater);

    for (auto const& x : r) {
        QString callsign;
        qDebug() << x.first << x.second.callsign;
    }

    qDebug() << "count: " << r.size();

    //return a.exec();
}
