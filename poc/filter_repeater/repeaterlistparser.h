#ifndef JSON_H
#define JSON_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QTextStream>
#include <QDebug>
#include <QFile>
#include "main.h"

typedef std::map<int, Repeater> RptrList;

QString createDataPath();

class repeaterListParser : public QObject
{
    Q_OBJECT
    int count = 0;
    int skip = 0;

public:
    explicit repeaterListParser(QObject *parent = nullptr);

    enum SaveFormat {
        Json, Binary
    };
    RptrList loadJson(QFile *file, repeaterListParser::SaveFormat saveFormat, RptrList &repeater);
    RptrList read(const QJsonArray &repeaterListParser, RptrList &repeater);
    QByteArray loadAndReturnJson(QFile *file, repeaterListParser::SaveFormat saveFormat);
};

#endif // JSON_H
