#ifndef REPEATERLISTMODEL_H
#define REPEATERLISTMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include "repeaterlistparser.h"
#include "main.h"

class RepeaterListModel : public QAbstractTableModel
{

    Q_OBJECT

public:

    //MyModel(QObject *parent = nullptr);
    RepeaterListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void beginRst();
    void endRst();

    int updateVector();
    void setHeader();

    repeaterListParser repeaterParser;
    RptrList repeater;
    QVector<Repeater> repeaterVector;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    int filterRepeaterList(QString search, double lat, double lng, double radius);

signals:
    //void dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight);
    void dataFiltered(int NumberOfRecords, int numberOfRecordsIgnored);

};

#endif // REPEATERLISTMODEL_H
