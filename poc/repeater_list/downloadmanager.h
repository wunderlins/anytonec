#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

// https://code.qt.io/cgit/qt/qtbase.git/plain/examples/network/download/main.cpp?h=5.13

#include <QtCore>
#include <QtNetwork>

#include <cstdio>

QT_BEGIN_NAMESPACE
class QSslError;
QT_END_NAMESPACE

using namespace std;

class DownloadManager: public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QVector<QNetworkReply *> currentDownloads;
    QString downloadUrl;

signals:
    void dataDownloaded();

public:
    DownloadManager();
    void go(QString url);
    void doDownload(const QUrl &url);
    static QString saveFileName(const QUrl &url);
    bool saveToDisk(const QString &filename, QIODevice *data);
    static bool isHttpRedirect(QNetworkReply *reply);

public slots:
    void execute(QString url);
    void downloadFinished(QNetworkReply *reply);
    void sslErrors(const QList<QSslError> &errors);
};

#endif // DOWNLOADMANAGER_H
