#include "main.h"
#include "repeaterlistparser.h"
#include <memory>
#include <QVector>

repeaterListParser::repeaterListParser(QObject *parent) : QObject(parent)
{
    excludeSystem = QStringList();
}

RptrList repeaterListParser::read(const QJsonArray &json, RptrList &repeater)
{
    /*
    if (json.contains("player") && json["player"].isObject())
        mPlayer.read(json["player"].toObject());

    if (json.contains("levels") && json["levels"].isArray()) {
        QJsonArray levelArray = json["levels"].toArray();
        mLevels.clear();
        mLevels.reserve(levelArray.size());
        for (int levelIndex = 0; levelIndex < levelArray.size(); ++levelIndex) {
            QJsonObject levelObject = levelArray[levelIndex].toObject();
            Level level;
            level.read(levelObject);
            mLevels.append(level);
        }
    }
    */
    count = 0;
    skip = 0;

    // reset the repeater lsit
    repeater.clear();

    /**
     * repeater object
     *
     * {
     *  "repeaterid": "21300022",
     *  "callsign": "",
     *  "hardware": "Windows:BlueDV",
     *  "firmware": "1.0.0.9551-DVMEGA_HR3.26",
     *  "tx": "439.9500",
     *  "rx": "439.9500",
     *  "colorcode": "1",
     *  "status": "4",
     *  "lastKnownMaster": "2141",
     *  "lat": "42.506001",
     *  "lng": "1.518100",
     *  "city": "Somewhere",
     *  "website": "www.pa7lim.nl",
     *  "pep": null,
     *  "gain": null,
     *  "agl": "1",
     *  "priorityDescription": null,
     *  "description": null,
     *  "last_updated": "2019-07-09 20:29:37"
     * },
     */
    QVector<QString> firmwares(0); firmwares.reserve(200);
    QVector<QString> hardwares(0); hardwares.reserve(200);

    /*
    QVector<QString> excludeSystem = {
        "pi-star", "dvmega", "zumspot", "mmdvm", "openspot", "bluedv"
    };
    */

    qDebug() << "excludeSystems: " << excludeSystem.size() << excludeSystem;

    for (int i = 0; i < json.size(); ++i) {
        bool cont = false;
        QJsonObject obj = json[i].toObject();

        // filter out unusable objects
        auto lat = obj.value("lat").toVariant().toFloat();
        auto lng = obj.value("lng").toVariant().toFloat();
        QString callsign = obj.value("callsign").toString();
        QString firmware = obj.value("firmware").toString();
        QString hardware = obj.value("hardware").toString();
        if (lat == 0) {
            skip++;
            continue;
        }
        if (lng == 0) {
            skip++;
            continue;
        }
        if (callsign.compare("") == 0) {
            skip++;
            continue;
        }

        /*
        // remeber all firmware names
        if (firmware != "" && firmwares.indexOf(firmware) == -1)
            firmwares.append(firmware);

        // remeber all hardware names
        if (hardware != "" && hardwares.indexOf(hardware) == -1)
            hardwares.append(hardware);
        */

        int s = excludeSystem.size();
        for (int i=0; i<s; i++) {
            //qDebug() << excludeSystem.at(i);
            if (firmware.contains(excludeSystem.at(i), Qt::CaseInsensitive)) {
                cont = true;
                //qDebug() << "skipping fw" << firmware;
                goto cnt;
            }
        }
        for (int i=0; i<s; i++) {
            if (hardware.contains(excludeSystem.at(i), Qt::CaseInsensitive)) {
                cont = true;
                //qDebug() << "skipping hw" << hardware;
                goto cnt;
            }
        }

        cnt:
        if (cont) {
            skip++;
            continue;
        }

        int id = obj.value("repeaterid").toVariant().toInt();
        Repeater r(id);
        //qDebug() << "id" << id;

        r.callsign = callsign;
        r.firmware = obj.value("firmware").toString();
        r.hardware = obj.value("hardware").toString();

        r.rx = obj.value("rx").toVariant().toDouble();
        r.tx = obj.value("tx").toVariant().toDouble();
        r.colorcode = obj.value("colorcode").toVariant().toInt();
        r.status = obj.value("status").toVariant().toInt();
        r.lastKnownMaster = obj.value("lastKnownMaster").toVariant().toInt();

        r.lat = obj.value("lat").toVariant().toDouble();
        r.lng = obj.value("lng").toVariant().toDouble();
        r.agl = obj.value("agl").toVariant().toInt();
        r.loc.setLatitude(r.lat);
        r.loc.setLongitude(r.lng);

        r.city = obj.value("city").toString();
        r.website = obj.value("website").toString();
        r.pep = obj.value("pep").toString();
        r.gain = obj.value("gain").toString();

        r.priorityDescription = obj.value("priorityDescription").toString();
        r.description = obj.value("description").toString();

        repeater.insert(std::pair<int, Repeater>(id, r)); // insert ( std::pair<char,int>('a',100) )
        /*
        qDebug() << obj.value("lat").toVariant().toFloat() << ":"
                 << obj.value("lng").toVariant().toFloat() << "\t"
                 << " " << obj.value("callsign").toString();
        */
        count++;

    }

    /*
    for (int i = 0; i < firmwares.size(); ++i) {
        qDebug() << firmwares.at(i);
    }
    for (int i = 0; i < hardwares.size(); ++i) {
        qDebug() << hardwares.at(i);
    }
    */

    qDebug() << "json::read(): count: " << repeater.size();
    qDebug() << "json::read(): skipped: " << skip;

    emit dataLoaded(count, skip);

    return repeater;
}

QByteArray repeaterListParser::loadAndReturnJson(QFile *file)
{
    // QFile loadFile(file);

    count = 0;
    qDebug() << "File name: " << file->fileName();

    if (!file->open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return nullptr;
    }

    return file->readAll();
}

RptrList repeaterListParser::loadJson(QFile *file, repeaterListParser::SaveFormat saveFormat, RptrList &repeater)
{
    // QFile loadFile(file);

    count = 0;
    qDebug() << "File name: " << file->fileName();

    if (!file->open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return RptrList();
    }

    QByteArray saveData = file->readAll();

    QJsonDocument loadDoc(saveFormat == Json
        ? QJsonDocument::fromJson(saveData)
        : QJsonDocument::fromBinaryData(saveData));

    read(loadDoc.array(), repeater);
    //qDebug() << "repeater: " << repeater.size();

    /*
    QTextStream(stdout) << "Loaded save for "
                        << loadDoc["player"]["name"].toString()
                        << " using "
                        << (saveFormat != Json ? "binary " : "") << "JSON...\n";
    */

    //qDebug() << "Number of repeaters: " << count;

    loaded = true;

    return repeater;
}
