#ifndef REPEATERLISTPARSER_H
#define REPEATERLISTPARSER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QTextStream>
#include <QDebug>
#include <QFile>
#include "main.h"

typedef std::map<int, Repeater> RptrList;

QString createDataPath();

class repeaterListParser : public QObject
{
    Q_OBJECT
    int count = 0;
    int skip = 0;

public:
    explicit repeaterListParser(QObject *parent = nullptr);

    bool loaded = false;
    QStringList excludeSystem;

    enum SaveFormat {
        Json, Binary
    };

    RptrList loadJson(QFile *file, repeaterListParser::SaveFormat saveFormat, RptrList &repeater);
    RptrList read(const QJsonArray &repeaterListParser, RptrList &repeater);
    QByteArray loadAndReturnJson(QFile *file);

signals:
    //void dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight);
    void dataLoaded(int NumberOfRecords, int numberOfRecordsIgnored);

};

#endif // REPEATERLISTPARSER_H
