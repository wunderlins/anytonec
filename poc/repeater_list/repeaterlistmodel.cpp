#include "repeaterlistmodel.h"
#include "repeaterlistmodel.h"
#include <math.h>

RepeaterListModel::RepeaterListModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

int RepeaterListModel::rowCount(const QModelIndex & /*parent*/) const
{
   return repeaterVector.size();
}

int RepeaterListModel::columnCount(const QModelIndex & /*parent*/) const
{
    if (repeater.size() == 0)
        return 0;
    return 13;
}

/**
 * repeater object
 *
 * {
 *  "repeaterid": "21300022",
 *  "callsign": "",
 *  "hardware": "Windows:BlueDV",
 *  "firmware": "1.0.0.9551-DVMEGA_HR3.26",
 *  "tx": "439.9500",
 *  "rx": "439.9500",
 *  "colorcode": "1",
 *  "status": "4",
 *  "lastKnownMaster": "2141",
 *  "lat": "42.506001",
 *  "lng": "1.518100",
 *  "city": "Somewhere",
 *  "website": "www.pa7lim.nl",
 *  "pep": null,
 *  "gain": null,
 *  "agl": "1",
 *  "priorityDescription": null,
 *  "description": null,
 *  "last_updated": "2019-07-09 20:29:37"
 * },
 */
QVariant RepeaterListModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole) {
        if (repeaterVector.size() == 0)
            return QVariant();

        auto r = repeaterVector.at(index.row());
        //qDebug() << "Reading row: " << index.row();

        switch (index.column()) {
            case 0: return r.repeaterid;
            case 1: return r.callsign;
            case 2: return r.tx;
            case 3: return r.rx;
            case 4: return r.colorcode;
            case 5: return r.hardware;
            case 6: return r.firmware;
            case 7: return r.status;
            case 8: return r.lastKnownMaster;
            case 9: return r.lat;
            case 10: return r.lng;
            case 11: return r.city;
            case 12: return r.website;
        }

        return "...";
    }
    return QVariant();
}

void RepeaterListModel::beginRst()
{
    beginResetModel();
}

void RepeaterListModel::endRst()
{
    endResetModel();
    //qDebug() << "RepeaterListModel::endRst(): " << repeaterVector.size();
}

int RepeaterListModel::updateVector()
{
    repeaterVector.clear();
    for (auto const& x : repeater) {
        //qDebug() << x.first << x.second.callsign;
        repeaterVector.push_back(x.second);
        //qDebug() << x.second.callsign;
    }

    emit dataFiltered(repeaterVector.size(), 0);

    return repeaterVector.size();
}

QVariant RepeaterListModel::headerData(int section, Qt::Orientation orientation,
                                int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0: return tr("ID");
            case 1: return tr("Callsign");
            case 2: return tr("TX");
            case 3: return tr("RX");
            case 4: return tr("ColorC");
            case 5: return tr("Hardware");
            case 6: return tr("Firmware");
            case 7: return tr("Status");
            case 8: return tr("lastKnownMaster");
            case 9: return tr("Lat");
            case 10: return tr("Long");
            case 11: return tr("City");
            case 12: return tr("Website");
        }
    }
    return section;
}

int RepeaterListModel::filterRepeaterList(QString search, double lat, double lng, double radius)
{
    qDebug() << "searching for: " << search << lat << lng << radius;
    bool validLocationData = true;

    search = search.trimmed();

    qDebug() << search.isEmpty() << search.compare("");

    if (lat > 180 || lat < -180) validLocationData = false;
    if (lng > 180 || lng < -180) validLocationData = false;
    if (radius == 0) validLocationData = false;

    QGeoPolygon poly;
    if (validLocationData) {
        auto center = QGeoCoordinate(lat, lng);
        double distToCorner = radius * sqrt(2) * 1000; // in meters
        QList<QGeoCoordinate> coords = {
            center.atDistanceAndAzimuth(distToCorner, 45),
            center.atDistanceAndAzimuth(distToCorner, 135),
            center.atDistanceAndAzimuth(distToCorner, 225),
            center.atDistanceAndAzimuth(distToCorner, 315),
            center.atDistanceAndAzimuth(distToCorner, 45)
        };
        poly.setPath(coords);

        /*
        qDebug() << "Distance from top left to bottom left" << coords[0].distanceTo(coords[1]);
        qDebug() << "poly 0" << coords[0].latitude() << coords[0].longitude() << endl
                 << "poly 1" << coords[1].latitude() << coords[1].longitude() << endl
                 << "poly 2" << coords[2].latitude() << coords[2].longitude() << endl
                 << "poly 3" << coords[3].latitude() << coords[3].longitude();
        */

        QGeoCoordinate basel(47.5596, 7.5886);
        qDebug() << "inside:" << poly.contains(basel);
    }

    beginRst();
    repeaterVector.clear();

    for (auto const& x : repeater) {
        const Repeater *r = &x.second;

        // filter geographically
        if (validLocationData && !poly.contains(r->loc)) {
            //qDebug() << "Skipping location";
            continue;
        }

        // filter for name and callsign
        if (!search.isEmpty()) {
            qDebug() << "comparing";
            // firmware.contains(excludeSystem.at(i), Qt::CaseInsensitive)
            if (! r->callsign.contains(search, Qt::CaseInsensitive) &&
                ! r->city.contains(search, Qt::CaseInsensitive)) {
                qDebug() << "Skipping search" << search;
                continue;
            }
        }

        repeaterVector.push_back(*r);
    }
    endRst();

    emit dataFiltered(repeaterVector.size(), (repeater.size() - repeaterVector.size()));

    return repeaterVector.size();
}

