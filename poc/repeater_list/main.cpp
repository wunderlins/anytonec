#include "mainwindow.h"
#include <QApplication>

QSettings cfg(APP_COMPANY, APP_NAME);

QString createDataPath() {
    //QString location = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString location = QCoreApplication::applicationDirPath();
    //location.append("/../https/");

    QDir dir(location);
    if (!dir.exists()) {
        if (!dir.mkpath(location)) {
            qDebug() << "Error, failed to create data direcotry";
            return nullptr;
        }
    }

    return location;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // initialize namespaces for our config object to be used various system
    // config subsystems.
    QCoreApplication::setOrganizationName(APP_COMPANY);
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setOrganizationDomain(APP_DOMAIN);

    // create window
    MainWindow w;
    w.show();

    // start application
    return a.exec();
}
