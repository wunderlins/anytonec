#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QQmlEngine>
#include <QQuickItem>
#include "main.h"
#include "repeaterlistmodel.h"
#include "repeaterlistparser.h"
#include "downloadmanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    RepeaterListModel rptrListModel;

    void repeaterListReset();
    void enableFilter(bool state);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

    void centerMap(double lat, double lng);

protected slots:
    void restoreHeaderState();

private slots:
    void on_actionLoadData_triggered();
    void on_actionResetData_triggered();
    void on_btnFilter_clicked();
    void on_btnReset_clicked();
    void on_excludeFirmware_stateChanged(int arg1);
    void on_search_returnPressed();
    void on_excludeFirmwareList_textChanged();
    void on_excludeFirmwareList_modificationChanged(bool arg1);
    void on_tabWidget_currentChanged(int index);
    void on_bmListUrl_editingFinished();

    void dataLoaded(int records, int ignored);
    void dataFiltered(int records, int ignored);

    void on_actionDownload_triggered();


    void on_lat_returnPressed();

    void on_lng_returnPressed();

    void on_radius_returnPressed();

private:
    Ui::MainWindow *ui;
    bool filterHasChanged = false;
    int lastTabRepeater = 0;
    DownloadManager manager;

    QQmlEngine *engine;
    QObject *root;

};

#endif // MAINWINDOW_H
