#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    // restore ui configuration
    //QVariant filerEnable = settings::cfgGet("config/repeaterFilterEnable", 1);

    qDebug() << "Config file location " << cfg.fileName();

    QVariant cfgBmRepeaterList = cfg.value("url/BmRepeaterList", "https://api.brandmeister.network/v1.0/repeater/?action=LIST");
    QVariant cfgFilerEnable    = cfg.value("config/repeaterFilterEnable", 1);
    QVariant cfgFiler          = cfg.value("config/repeaterFilter", "pi-star, dvmega, zumspot, mmdvm, openspot, bluedv");

    // setup ui
    ui->setupUi(this);
    ui->excludeFirmware->setCheckState( (cfgFilerEnable.toInt() == 1) ? Qt::Checked : Qt::Unchecked);
    ui->excludeFirmwareList->setPlainText(cfgFiler.toString());
    ui->bmListUrl->setText(cfgBmRepeaterList.toString());

    // search parameter
    QString cfgSearch = cfg.value("search/search", "").toString();
    QString cfgLat    = cfg.value("search/lat", "").toString();
    QString cfgLng    = cfg.value("search/lng", "").toString();
    QString cfgRadius = cfg.value("search/radius", "").toString();

    if (cfgSearch == "0") cfgSearch = "";
    if (cfgLat == "0")    cfgLat = "";
    if (cfgLng == "0")    cfgLng = "";
    if (cfgRadius == "0") cfgRadius = "";

    ui->search->setText(cfgSearch);
    ui->lat->setText(cfgLat);
    ui->lng->setText(cfgLng);
    ui->radius->setText(cfgRadius);


    // this is a hack, restoring column widths inside the constructor does not seem to work
    QTimer::singleShot(0, this, SLOT(restoreHeaderState()));

    restoreGeometry(cfg.value("data/geometry").toByteArray());
    restoreState(cfg.value("data/windowState").toByteArray());


    // enable data signals
    filterHasChanged = false;
    connect(&rptrListModel.repeaterParser, &repeaterListParser::dataLoaded,
            this, &MainWindow::dataLoaded);
    connect(&rptrListModel, &RepeaterListModel::dataFiltered,
            this, &MainWindow::dataFiltered);

    connect(&manager, &DownloadManager::dataDownloaded,
            this, &MainWindow::on_actionLoadData_triggered);


    // read configuration
    enableFilter(ui->excludeFirmware->checkState());
}

void MainWindow::centerMap(double lat, double lng) {
    QGeoCoordinate center(lat, lng);
    engine = ui->mapWidget->engine();
    root = ui->mapWidget->rootObject();
    if (root == nullptr) return;

    QObject *mapCanvas = root->findChild<QObject*>("mapCanvas");
    if (mapCanvas != nullptr) {
        qDebug() << mapCanvas;
        mapCanvas->setProperty("center", QVariant::fromValue(center));
    }


}


void MainWindow::restoreHeaderState() {
    QVariant cfgColumnWidth    = cfg.value("data/ColumnWidth", "");
    ui->repeaterList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->repeaterList->horizontalHeader()->setStretchLastSection(true);
    bool state = ui->repeaterList->horizontalHeader()->restoreState(cfgColumnWidth.toByteArray());
    qDebug() << "Restore header state: " << state;
}

MainWindow::~MainWindow()
{
    QByteArray MyArray = ui->repeaterList->horizontalHeader()->saveState();
    //qDebug() << "state " << MyArray;
    cfg.setValue("data/ColumnWidth", MyArray);

    cfg.setValue("data/geometry", saveGeometry());
    cfg.setValue("data/windowState", saveState());

    delete ui;
}


void MainWindow::enableFilter(bool state) {
    if (state) {
        QString text = ui->excludeFirmwareList->toPlainText();
        QStringList list = text.split(",", QString::SkipEmptyParts);
        QStringList list2;
        for(auto& str : list)
            list2 << str.trimmed().toLower();

        rptrListModel.repeaterParser.excludeSystem = list2;
        qDebug() << ">>>" << rptrListModel.repeaterParser.excludeSystem;

    } else {
        //cfg.setValue("config/repeaterFilterEnable", 0);
        rptrListModel.repeaterParser.excludeSystem.clear();
        qDebug() << ">>> no exclude Filter";
    }

    // setup repeaterList model
    ui->repeaterList->setModel(&rptrListModel);

    // load repeaterList Data
    on_actionLoadData_triggered();
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    qDebug() << "MainWindow::downloadProgress(" << bytesReceived << ", " << bytesTotal << ")";
}


void MainWindow::repeaterListReset()
{
    rptrListModel.beginRst();
    rptrListModel.repeaterVector.clear();
    rptrListModel.repeater.clear();
    rptrListModel.endRst();
}

void MainWindow::on_actionLoadData_triggered()
{
    // json file is located in the application directory
    QString location = createDataPath();
    if (location == nullptr) {
        return;
    }

    location.append("/");
    location.append(REPEATER_LIST_FILE);
    if(!QFileInfo(location).exists()){
        qDebug() << "++++ no repeater file, not loading";
        return;
    }

    QFile f(location);

    rptrListModel.beginRst();
    // parse the repeater list
    rptrListModel.repeaterParser.loadJson(&f, repeaterListParser::Json, rptrListModel.repeater);
    // update our vector to the map of repeaters;
    rptrListModel.updateVector();
    // signal an update
    rptrListModel.endRst();

    ui->actionLoadData->setEnabled(true);
    ui->actionResetData->setEnabled(true);

    on_btnFilter_clicked();

    qDebug() << "Number of Repeaters" << rptrListModel.repeater.size();
}

void MainWindow::on_actionResetData_triggered()
{
    repeaterListReset();
}

void MainWindow::on_btnFilter_clicked()
{
    // do we have data? if not return
    if (rptrListModel.repeaterParser.loaded == false)
        return;

    auto search = ui->search->text();
    double lat = ui->lat->text().toDouble();
    double lng = ui->lng->text().toDouble();
    double radius = ui->radius->text().toDouble();

    cfg.setValue("search/search", search);
    cfg.setValue("search/lat", lat);
    cfg.setValue("search/lng", lng);
    cfg.setValue("search/radius", radius);

    rptrListModel.filterRepeaterList(search, lat, lng, radius);

    qDebug() << "==== Filter set " << lat << ", " << lng;

    if (lat > 0) {
        centerMap(lat, lng);
    }

    //ui->statusBar->showMessage("Repeater: " + QString::number(rptrListModel.repeaterVector.size()));
}

void MainWindow::on_btnReset_clicked()
{
    ui->search->setText("");
    ui->lat->setText("");
    ui->lng->setText("");
    ui->radius->setText("");

    rptrListModel.filterRepeaterList(nullptr, -200, -200, 0);

    //ui->statusBar->showMessage("Repeater: " + QString::number(rptrListModel.repeaterVector.size()));
}

void MainWindow::on_excludeFirmware_stateChanged(int arg1)
{
    filterHasChanged = true;
    //enableFilter(arg1);
}

void MainWindow::dataLoaded(int records, int ignored)
{
    qDebug() << "++++ MainWindow::dataLoaded(" << records << ", " << ignored << ")";

    // remeber the file size, the web server does not give us a Content-length

    QString location = createDataPath();
    if (location == nullptr) {
        return;
    }
    location.append("/");
    location.append(REPEATER_LIST_FILE);
    QFile f(location);
    auto size = f.size();
    cfg.setValue("data/BmRepeaterListLength", size);

    //on_actionLoadData_triggered();

    /*
    ui->statusBar->showMessage("Data loaded, records " + QString::number(records) +
                               " (" + QString::number(ignored) + " ignored)");
    */
}

void MainWindow::dataFiltered(int records, int ignored)
{
    // display status
    ui->statusBar->showMessage("Data filtered, showing " + QString::number(records) +
                               " (" + QString::number(ignored) + " ignored)");
}

void MainWindow::on_excludeFirmwareList_textChanged()
{
    // qDebug() << "text Changed";
    filterHasChanged = true;
}


void MainWindow::on_excludeFirmwareList_modificationChanged(bool arg1)
{

}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    qDebug() << index;
    // save config
    if (lastTabRepeater == 1 && filterHasChanged == true) {
        cfg.setValue("config/repeaterFilterEnable", (ui->excludeFirmware->checkState() == Qt::Checked) ? 1 : 0 );
        cfg.setValue("config/repeaterFilter", ui->excludeFirmwareList->toPlainText());

        // update filters
        enableFilter((ui->excludeFirmware->checkState() == Qt::Checked) ? 1 : 0);
        filterHasChanged = false;
    }


    lastTabRepeater = index;
}

void MainWindow::on_bmListUrl_editingFinished()
{
    cfg.setValue("url/BmRepeaterList", ui->bmListUrl->text());
}

void MainWindow::on_actionDownload_triggered()
{
    QString url = cfg.value("url/BmRepeaterList").toString();
    qDebug() << "Downloading Repeater List from:" << url;

    manager.execute(url);

    /*
    connect(manager.reply, &QNetworkReply::downloadProgress,
            this, &MainWindow::downloadProgress);
    */
    //manager.execute(url);
}

void MainWindow::on_search_returnPressed()
{
    on_btnFilter_clicked();
}

void MainWindow::on_lat_returnPressed()
{
    on_btnFilter_clicked();
}

void MainWindow::on_lng_returnPressed()
{
    on_btnFilter_clicked();
}

void MainWindow::on_radius_returnPressed()
{
    on_btnFilter_clicked();
}
