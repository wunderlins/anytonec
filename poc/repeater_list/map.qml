import QtQuick 2.0
import QtQuick.Window 2.0
import QtLocation 5.6
import QtPositioning 5.6

Item {
    width: 512
    height: 512
    visible: true

    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        PluginParameter { name: "osm.mapping.cache.disk.cost_strategy"; value: "bytesize" }
        PluginParameter { name: "osm.mapping.cache.disk.size"; value: 500 }
        PluginParameter { name: "osm.useragent"; value: "repeater_list 1.0"}

        // specify plugin parameters if necessary
        // PluginParameter {
        //     name:
        //     value:
        // }
    }

    Map {
        anchors.fill: parent
        plugin: mapPlugin
        center: QtPositioning.coordinate(59.91, 10.75) // Oslo
        zoomLevel: 14
        objectName: "mapCanvas"
    }
}
