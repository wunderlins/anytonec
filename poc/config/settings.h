#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QCoreApplication>
#include <QVariant>
#include <QString>

static QSettings cfg("WunderlinSoft", "Config");
void cfgInit();
QVariant cfgGet(QString name);
void cfgSet(QString name, QVariant value);

#endif // SETTINGS_H
