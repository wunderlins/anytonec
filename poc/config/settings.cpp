#include "settings.h"

void cfgInit() {
    // initialize namespaces for our config object to be used various system
    // config subsystems.
    QCoreApplication::setOrganizationName("WunderlinSoft");
    QCoreApplication::setOrganizationDomain("wunderlin.net");
    QCoreApplication::setApplicationName("Config");
}

QVariant cfgGet(QString name)
{
    return cfg.value(name);
}

void cfgSet(QString name, QVariant value)
{
    cfg.setValue(name, value);
}
