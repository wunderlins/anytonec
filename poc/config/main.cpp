#include <QCoreApplication>
#include <QDebug>
#include "settings.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    cfgInit();

    cfgSet("bmUrl", "https://brandmeister.network/api/...");
    qDebug() << "bmUrl" << cfgGet("bmUrl").toString();

    return a.exec();
}
