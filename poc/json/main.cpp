#include <QCoreApplication>
#include "repeaterlistparser.h"
#include "main.h"

QString createDataPath() {
    QString location = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    location.append("/../https/");

    QDir dir(location);
    if (!dir.exists()) {
        if (!dir.mkpath(location)) {
            qDebug() << "Error, failed to create data direcotry";
            return nullptr;
        }
    }

    return location;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString location = createDataPath();
    if (location == nullptr) {
        return 1;
    }

    location.append("/repeater_list.json");
    QFile f(location);

    repeaterListParser j;
    j.loadJson(&f, repeaterListParser::Json);

    return a.exec();
}
