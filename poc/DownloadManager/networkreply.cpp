#include "networkreply.h"

NetworkReply::NetworkReply(QObject *parent) : QNetworkReply(parent) {}
void NetworkReply::setUrl(QString url) {this->_url = url;}
void NetworkReply::setFile(QString file) {this->_file = file;}
QString NetworkReply::getUrl() {return this->_url;}
QString NetworkReply::getFile() {return this->_file;}
