#ifndef NETWORKREPLY_H
#define NETWORKREPLY_H

#include <QNetworkReply>

class NetworkReply : public QNetworkReply
{
    QString _url;
    QString _file;

public:
    explicit NetworkReply(QObject *parent = nullptr);

    void setUrl(QString url);
    void setFile(QString file);
    QString getUrl();
    QString getFile();
};

#endif // NETWORKREPLY_H
