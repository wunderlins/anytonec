#include "downloadmanager.h"

QString createDataPath() {
    //QString location = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString location = QCoreApplication::applicationDirPath();
    //location.append("/../https/");

    QDir dir(location);
    if (!dir.exists()) {
        if (!dir.mkpath(location)) {
            qDebug() << "Error, failed to create data direcotry";
            return nullptr;
        }
    }

    return location;
}

DownloadManager &DownloadManagerInit(QObject *parent, QString url, QString file)
{
    DownloadManager *dm = new DownloadManager(parent);
    dm->add(url, file);
    return *dm;
}

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{
    qDebug() << "DownloadManager::DownloadManager";
    this->parent = parent;

    connect(&manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(downloadFinished(QNetworkReply*)));

}

void DownloadManager::add(QString url, QString file)
{
    // construct url
    qDebug() << "DownloadManager::add" << url << ", " << file;
    QUrl u = QUrl::fromEncoded(url.toLocal8Bit());

    QNetworkRequest request(u);
    QNetworkReply *reply = manager.get(request);

#if QT_CONFIG(ssl)
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)),
            SLOT(sslErrors(QList<QSslError>)));
#endif

    // connect signals
    connect(reply, &QNetworkReply::downloadProgress,
            this, &DownloadManager::downloadProgress);
    connect(reply, &QIODevice::readyRead,
            this, &DownloadManager::httpReadyRead);


    currentDownloads = reply;
    currentFile = file;
}

bool DownloadManager::isHttpRedirect(QNetworkReply *reply)
{
    qDebug() << "DownloadManager::isHttpRedirect" << reply;
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode == 301 || statusCode == 302 || statusCode == 303
            || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

void DownloadManager::downloadFinished(QNetworkReply *reply) {
    qDebug() << "DownloadManager::downloadFinished" << reply;

    QUrl url = reply->url();
    if (reply->error()) {
        fprintf(stderr, "Download of %s failed: %s\n",
                url.toEncoded().constData(),
                qPrintable(reply->errorString()));
        reply->deleteLater();
        return;
    }

    if (isHttpRedirect(reply)) {
        fputs("Request was redirected.\n", stderr);
        reply->deleteLater();
        return;
    }

    // save content to file
    qDebug() << reply->url() << ": " << currentFile;
    QFile *file = new QFile(createDataPath().append("/").append(currentFile));

    if (!file->open(QIODevice::WriteOnly)) {
        fprintf(stderr, "Could not open %s for writing: %s\n",
                qPrintable(currentFile),
                qPrintable(file->errorString()));
        return;
    }

    file->write(reply->readAll());
    file->close();

    qDebug() << file->fileName();

    emit finished(file);

    // deallocate stuff
    currentFile = nullptr;
    reply->deleteLater();
}

void DownloadManager::sslErrors(const QList<QSslError> &sslErrors) {
    qDebug() << "DownloadManager::sslErrors" << sslErrors;
#if QT_CONFIG(ssl)
    for (const QSslError &error : sslErrors)
        fprintf(stderr, "SSL error: %s\n", qPrintable(error.errorString()));
#else
    Q_UNUSED(sslErrors);
#endif
}

void DownloadManager::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    qDebug() << "DownloadManager::downloadProgress" << bytesReceived << ", " << bytesTotal;
    emit progress(bytesReceived, bytesTotal);
}

void DownloadManager::httpReadyRead()
{
    qDebug() << "DownloadManager::httpReadyRead";

}

