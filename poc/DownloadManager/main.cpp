#include <QCoreApplication>
#include "downloadmanager.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    /*
    DownloadManager manager(&a);
    manager.add("https://api.brandmeister.network/v1.0/repeater/?action=LIST", "myfile.html");
    */


    // interface
    //
    // 1. provide (url, fielname, [temp directory])
    QString url = "https://api.brandmeister.network/v1.0/repeater/?action=LIST";
    QString file = "myfile.html";
    DownloadManager *m = &DownloadManagerInit(&a, url, file);


    // 2. connect signal handlers for (progress, finished, error) to
    //    active download
    // 3. de-allocate shit
    // 4. provide pointer to result file on success

    return a.exec();
}
