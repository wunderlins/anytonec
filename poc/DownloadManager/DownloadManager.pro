QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle
CONFIG += file_copies

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        downloadmanager.cpp \
        downloadmanagerreply.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    downloadmanager.h \
    downloadmanagerreply.h


CONFIG(debug, debug|release) {
    OUT = $${OUT_PWD}/debug
} else {
    OUT = $${OUT_PWD}/release
}
win32 {
#    RC_ICONS = icons/app.ico

#    # copy libssl into windows build directory
#    EXTRA_BINFILES += \
#        $${_PRO_FILE_PWD_}/libs/libcrypto-1_1-x64.dll \
#        $${_PRO_FILE_PWD_}/libs/libgcc_s_seh-1.dll \
#        $${_PRO_FILE_PWD_}/libs/libssl-1_1-x64.dll \
#        $${_PRO_FILE_PWD_}/libs/libwinpthread-1.dll \
#        $${_PRO_FILE_PWD_}/libs/Qt5Core.dll \
#        $${_PRO_FILE_PWD_}/libs/Qt5Gui.dll \
#        $${_PRO_FILE_PWD_}/libs/Qt5Network.dll \
#        $${_PRO_FILE_PWD_}/libs/Qt5Positioning.dll \
#        $${_PRO_FILE_PWD_}/libs/Qt5Widgets.dll \
#        $${_PRO_FILE_PWD_}/libs/libstdc++-6.dll

#    EXTRA_BINFILES_WIN = $${EXTRA_BINFILES}
#    EXTRA_BINFILES_WIN ~= s,/,\\,g
#        DESTDIR_WIN = $${OUT}
#    DESTDIR_WIN ~= s,/,\\,g
#    for(FILE,EXTRA_BINFILES_WIN){
#                QMAKE_POST_LINK +=$$quote(cmd /c copy /y \""$${FILE}"\" $${DESTDIR_WIN} $$escape_expand(\r\n\t))
#    }

    COPIES += winlibs
    winlibs.files = $$files(libs/w64/*.dll)
    winlibs.path  = $$OUT
}
