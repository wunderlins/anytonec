#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QtCore>
#include <QtNetwork>

QT_BEGIN_NAMESPACE
class QSslError;
QT_END_NAMESPACE

using namespace std;

class DownloadManager : public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QObject *parent;
    QNetworkReply * currentDownloads;
    QString currentFile;

    bool isHttpRedirect(QNetworkReply *reply);

public:
    explicit DownloadManager(QObject *parent = nullptr);
    void add(QString url, QString file);

    // thread safe singelton implementation, see:
    // https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
    DownloadManager(DownloadManager const&) = delete;
    void operator=(DownloadManager const&)  = delete;

    // Note: Scott Meyers mentions in his Effective Modern
    //       C++ book, that deleted functions should generally
    //       be public as it results in better error messages
    //       due to the compilers behavior to check accessibility
    //       before deleted status

signals:
    void finished(QFile *file);
    void error();
    void progress(qint64 bytesReceived, qint64 bytesTotal);

public slots:
    void downloadFinished(QNetworkReply *reply);
    void sslErrors(const QList<QSslError> &errors);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void httpReadyRead();


};

DownloadManager &DownloadManagerInit(QObject *parent = nullptr,
                                     QString url = nullptr,
                                     QString file = nullptr);

#endif // DOWNLOADMANAGER_H
