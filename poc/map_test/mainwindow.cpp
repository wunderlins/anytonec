#include <QQmlEngine>
#include <QQuickItem>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QQmlEngine *engine = ui->mapWidget->engine();
    QQuickItem *root = ui->mapWidget->rootObject();
    root->setOpacity(0.5);
    //QQuickItem* map = engine->findChild<QQuickItem*>("mapWidget");
    //map->rootObject();
}

MainWindow::~MainWindow()
{
    delete ui;
}

