#ifndef GEO_H
#define GEO_H

#include <QObject>
#include <QGeoCoordinate>
#include <QGeoPolygon>

/*
Some polys from zones.kml:

    <Folder>
        <name>DMR Regionen</name>
        <open>1</open>
        <Placemark>
            <name>Basel</name>
            <styleUrl>#msn_ylw-pushpin5</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    8.11315280703122,47.85803002356662,0 7.74297871370532,47.78042723789942,0 6.983873289188008,47.54303279721161,0 6.786230311165271,47.19325509530869,0 7.163990779651106,47.00327993785141,0 8.121203904905345,47.48480407657253,0 8.11315280703122,47.85803002356662,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Romandie</name>
            <styleUrl>#msn_ylw-pushpin4</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    6.691418030344909,47.42751672788073,0 7.266092732689047,47.1080036633111,0 7.065076879650986,46.7161406317729,0 7.417804938690371,46.21565103472,0 7.095964971226703,45.4897024385949,0 6.0737158333168,45.94583997022474,0 5.918323546382826,46.4832227555158,0 6.429807919406927,46.98694090024772,0 6.691418030344909,47.42751672788073,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Mittelland</name>
            <styleUrl>#msn_ylw-pushpin2</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    6.540223019289934,46.81974323061319,0 7.191373632661218,47.28375263524347,0 8.262391724151964,47.54106028413765,0 8.95697648068667,46.79100851852952,0 7.905845147571635,46.39268555316836,0 7.204924268657402,46.29755314774381,0 6.540223019289934,46.81974323061319,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Ostschweiz</name>
            <styleUrl>#msn_ylw-pushpin3</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    8.021711767334821,47.78439256521388,0 8.895215232300043,47.95749936000156,0 9.398091817858214,47.91994448483638,0 9.934964074635374,47.54748997379182,0 9.730346602587366,46.99651834463836,0 8.786172988088779,46.81511039990063,0 8.074611331252457,47.56534545655741,0 8.021711767334821,47.78439256521388,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Valis</name>
            <styleUrl>#msn_ylw-pushpin</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    6.974385861434989,46.54987763750256,0 7.62096822285611,46.54878438682458,0 8.591844704395877,46.87106405448809,0 8.822206622636262,46.50947153643829,0 8.270542294828404,46.16967097751976,0 8.084611817439093,45.79829661648822,0 7.153341103138253,45.48737350142842,0 6.640415055148106,45.83522652669114,0 6.624228528916801,46.31526876663245,0 6.974385861434989,46.54987763750256,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Graubünden</name>
            <styleUrl>#msn_ylw-pushpin1</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    8.610269997967494,46.90044374247128,0 9.969548812447648,47.2111496977825,0 10.75923701372143,46.92281729559375,0 10.58825165345703,46.40789722031011,0 10.01653582215751,46.09709240378287,0 9.361061802760105,46.2897947803377,0 8.811040717186632,46.64665434530001,0 8.610269997967494,46.90044374247128,0
                </coordinates>
            </LineString>
        </Placemark>
        <Placemark>
            <name>Tessin/Gotthard</name>
            <styleUrl>#msn_ylw-pushpin0</styleUrl>
            <LineString>
                <tessellate>1</tessellate>
                <coordinates>
                    7.94955502893327,46.74420420181663,0 8.739235335785097,46.98850420444166,0 9.483481424952098,46.50439848330183,0 9.473289557609878,45.97430718304267,0 9.056576694539498,45.67144449002094,0 8.510721346452465,45.74572405443195,0 8.073488296153922,46.37779190194876,0 7.94955502893327,46.74420420181663,0
                </coordinates>
            </LineString>
        </Placemark>
    </Folder>


*/


class geo : public QObject
{
    Q_OBJECT
public:
    explicit geo(QObject *parent = nullptr);

signals:

public slots:
};

#endif // GEO_H
