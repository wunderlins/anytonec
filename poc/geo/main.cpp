#include <QCoreApplication>
#include <QDebug>
#include "geo.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    geo g();
    QGeoCoordinate basel(47.5596, 7.5886);
    QGeoCoordinate zurich(47.3769, 8.5417);

    QList<QGeoCoordinate> coords = {
        { 47.85803002356662, 8.11315280703122 },
        { 47.78042723789942, 7.74297871370532 },
        { 47.54303279721161, 6.983873289188008 },
        { 47.19325509530869, 6.786230311165271 },
        { 47.00327993785141, 7.163990779651106 },
        { 47.48480407657253, 8.121203904905345 },
        { 47.85803002356662, 8.11315280703122 }
    };

    QGeoPolygon poly(coords);
    bool basel_inside  = poly.contains(basel);
    bool zurich_inside = poly.contains(zurich);

    qDebug() << "Distanz von basel nach Zürich: " << basel.distanceTo(zurich);
    qDebug() << "Basel ist im poly:             " << basel_inside;
    qDebug() << "Zuerich ist im poly:           " << zurich_inside;

    return a.exec();
}
